//
//  ServiceManager.swift
//  NCYSchools
//

import Foundation

enum ServiceError: Error {
    case badUrl, networkError, badResponse, wrongData, requestError(message: String)
    
    var errorDescription: String? {
        switch self {
        case .badUrl:
            return "Incorrect url"
        case .networkError:
            return "Network error"
        case .badResponse:
            return "Bad response"
        case .wrongData:
            return "Wrong data"
        case .requestError(let message):
            return message
        }
    }
}

protocol SchoolService {
    func get<T:Codable>(completion: @escaping (_ result: Result<[T], ServiceError>) -> Void)
}

class DefaultSchoolService: SchoolService {
    
    private var endpointUrl: String
    private let defaultSession = URLSession(configuration: .default)
    
    init(endpointUrl: String) {
        self.endpointUrl = endpointUrl
    }
    
    func get<T:Codable>(completion: @escaping (Result<[T], ServiceError>) -> Void) {
        
        var pathComponent = ""
        
        switch T.self {
        case is School.Type: pathComponent = "s3k6-pzi2.json"
        case is SATScore.Type: pathComponent = "f9bf-2cp4.json"
        default: break
        }
        
        performRequest(pathComponent: pathComponent, completion: completion)
    }
    
    private func performRequest<T:Codable>(pathComponent: String, completion: @escaping (Result<[T], ServiceError>) -> Void) {
        guard let url = URL(string: endpointUrl)?.appendingPathComponent(pathComponent) else {
            completion(.failure(.badUrl))
            return
        }
        
        defaultSession.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(.requestError(message: error.localizedDescription)))
                return
            }
            
            guard let data = data,
                  let response = response as? HTTPURLResponse else {
                      completion(.failure(.networkError))
                      return
                  }
            
            if response.statusCode != 200 {
                completion(.failure(.badResponse))
                return
            }
            
            if let schools = try? JSONDecoder().decode([T].self, from: data) {
                completion(.success(schools))
            } else {
                completion(.failure(.wrongData))
            }
            
        }.resume()
    }
}
