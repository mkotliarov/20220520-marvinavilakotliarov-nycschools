//
//  DataStorage.swift
//  NCYSchools
//
//  Created by Mac User on 5/20/22.
//

import Foundation

class DataStorage {
    struct Static {
        fileprivate static var shared: DataStorage?
    }
    
    class var instance: DataStorage {
        if Static.shared == nil {
            Static.shared = DataStorage()
        }
        return Static.shared!
    }
    
    private let satScoresKey = "SatScoresKey"
    
    var satScores: [SATScore] {
        get {
            guard let data = UserDefaults.standard.data(forKey: satScoresKey),
                  let satScores = try? JSONDecoder().decode([SATScore].self, from: data) else {
                return [SATScore]()
            }
            return satScores
        }
        set {
            if let encoded = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(encoded, forKey: satScoresKey)
            }
        }
    }
}
