//
//  Scholl.swift
//  NCYSchools
//

import Foundation

struct School: Codable {
    
    var schoolName: String
    var dbn: String
    
    private enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case dbn = "dbn"
    }
}

struct SATScore: Codable {
    var schoolName: String
    var dbn: String
    var reading: String
    var math: String
    var writing: String
    
    private enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case dbn = "dbn"
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
    }
}
