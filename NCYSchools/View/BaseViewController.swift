//
//  BaseViewController.swift
//  NCYSchools
//

import UIKit

/// This is a base class to instantiate UIViewControllers using nib files of the same name.
/// Its main purpose is to create concrete objects and avoid optional references to UIViewControllers in client code.
/// Subclases can set their init methods and inject dependencies like ViewModels.

public class BaseViewController: UIViewController {
    
    // MARK: - View lifecycle

    public init() {
        let name = String(describing: type(of: self))
        let bundle = Bundle(for: BaseViewController.self)
        super.init(nibName: name, bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func alert(title title:String, message:String) {
        let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
