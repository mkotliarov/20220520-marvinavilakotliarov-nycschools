//
//  ViewController.swift
//  NCYSchools
//

import UIKit

class SchoolsViewController: BaseViewController {
    
    private(set) var viewModel: SchoolsViewModel
    private let cellId = "cellId"
    private var errorMessage = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    init(viewModel: SchoolsViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        loadData()
    }
    
    private func setupUI() {
        title = "Schools Directory"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    private func loadData() {
        viewModel.getData { [unowned self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(_):
                    if viewModel.schools.isEmpty { errorMessage = "Empty data" }
                case .failure(let error):
                    errorMessage = error.localizedDescription
                }
                self.tableView.reloadData()
            }
        }
    }
}

extension SchoolsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.isEmpty ? 1 : viewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        if viewModel.schools.isEmpty {
            cell.textLabel?.text = errorMessage
            cell.textLabel?.textAlignment = .center
        } else {
            cell.textLabel?.text = viewModel.schools[indexPath.row].schoolName
        }
        
        return cell
    }
}

extension SchoolsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.getSatScore(for: viewModel.schools[indexPath.row].dbn) { satScore in
            if let satScore = satScore {
                self.alert(title: satScore.schoolName,
                           message: "Math:\(satScore.math)\n" +
                                    "Reading:\(satScore.reading)\n" +
                                    "Writing:\(satScore.writing)")
            } else {
                self.alert(title: "Error", message: "No SAT scores found")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.schools.isEmpty ? tableView.frame.height : UITableView.automaticDimension
    }
}

