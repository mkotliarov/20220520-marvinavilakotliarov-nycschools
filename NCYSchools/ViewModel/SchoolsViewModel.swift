//
//  SchoolsViewModel.swift
//  NCYSchools
//

import Foundation

class SchoolsViewModel {
    
    private(set) var service: SchoolService
    private(set) var schools = [School]()
    
    init(service: SchoolService) {
        self.service = service
    }
    
    func getData(completion: ((_ result: Result<Bool, Error>) -> Void)? = nil) {
        service.get() { [unowned self] (result:Result<[School], ServiceError>) in
            switch result {
            case .success(let schools):
                self.schools = schools.sorted { $0.schoolName < $1.schoolName }
                completion?(.success(true))
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getSatScore(for dbn:String, completion: @escaping ((_ satScore: SATScore?) -> Void)) {
        if DataStorage.instance.satScores.isEmpty {
            service.get() { (result:Result<[SATScore], ServiceError>) in
                switch result {
                case .success(let satScores): DataStorage.instance.satScores = satScores
                case .failure(_): completion(nil)
                }
            }
        } else {
            completion(DataStorage.instance.satScores.first { $0.dbn == dbn })
        }
    }
}
