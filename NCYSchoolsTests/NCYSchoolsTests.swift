//
//  NCYSchoolsTests.swift
//  NCYSchoolsTests
//

import XCTest
@testable import NCYSchools

class NCYSchoolsTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testApiCallCompletes() throws {
        
        // given
        let service = DefaultSchoolService(endpointUrl: "https://data.cityofnewyork.us/resource/")
        let promise = expectation(description: "Completion handler invoked")
        var dataCount = 0
        var responseError: Error?
        
        // when
        service.get() { (result:Result<[School], ServiceError>) in
            switch result {
            case .success(let data): dataCount = data.count
            case .failure(let error): responseError = error
            }
            promise.fulfill()
        }
        wait(for: [promise], timeout: 5)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertNotEqual(dataCount, 0)
    }

}
